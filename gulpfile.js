const gulp = require('gulp'),
    bs = require('browser-sync'),
    sass = require('gulp-sass'),
    concat = require('gulp-concat'),
    minify = require('gulp-minify'),
    watch = require('gulp-watch');

const sourceDir = './src/',
    destDir = './dist/';

//Sass
gulp.task('sass', function () {   
    return gulp.src(sourceDir + 'sass/main.scss')     
            .pipe(sass()) 
            .pipe(gulp.dest(destDir + 'css/'))
});

//browser reload
gulp.task('browser-sync', function () {
    // Serve files from the root of this project
    bs.init({
        server: {
            baseDir: "./dist"
         }
    });
});

//concat
gulp.task('concat_js', function() {
  //An array of files is required for the correct order of contact
  return gulp.src([
        'node_modules/jquery/dist/jquery.js',
        'node_modules/bootstrap/dist/js/bootstrap.bundle.js',
        'node_modules/dragula/dist/dragula.js',
        'node_modules/datatables.net/js/jquery.dataTables.js',
        'node_modules/datatables.net-fixedcolumns/js/dataTables.fixedColumns.js',
        'node_modules/datatables.net-bs4/js/dataTables.bootstrap4.js',
        'node_modules/datatables.net-fixedcolumns-bs4/js/fixedColumns.bootstrap4.js',
        sourceDir + 'js/**/*.js'
    ])
    .pipe(concat('script.js'))
    .pipe(minify({
        ext:{
            src:'',
            min:'.min.js'
        },
        noSource: true}))
    .pipe(gulp.dest(destDir + 'js/'));
});

gulp.task('html', function() {
    gulp.src(sourceDir + 'index.html')
        .pipe(gulp.dest(destDir));
});

//watch
gulp.task('watch', ['browser-sync', 'sass'], function () {
    gulp.watch(sourceDir + 'sass/**/*.scss', ['sass']);    
    gulp.watch(sourceDir + 'js/*.js', ['concat_js']);  
    gulp.watch(sourceDir + './*.html', ['html']);  
    gulp.watch(sourceDir + '**/*.*', bs.reload);    
});

gulp.task('default', ['watch', 'sass', 'concat_js', 'html']);